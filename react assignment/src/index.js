import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';    
import 'bootstrap/dist/css/bootstrap.min.css';    
import UserData from './component/UserData';
import ShowData from './component/ShowData';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

const routing = (
  <Router>
  <div>
  <Route exact path="/" component={UserData} />
  <Route path="/detail" component={ShowData} />
  </div>
  </Router>
 )
 ReactDOM.render(routing, document.getElementById('root'))