import React, { Component } from 'react'
import './ShowData.css'
class ShowData extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             image:'',
             name:'',
             address:'',
             city:'',
             resume:'',
             array:[],
             ten:'',
             twelve:'',
             exp:'',
             skill:'',


        }
    }
    
    componentDidMount(){
let data=JSON.parse(localStorage.getItem("user"));

        console.log(data[0].name);
        this.setState({
            // image:data[3].photo
            array:data
        })
    }
    render() {
        const ad=this.state.array.map((res) =>
         
         <tbody key={res.phno}>
             <tr>
             <td style={{width:'120px'}}>
                 {
                     res.name
                 }
             </td>
             <td style={{width:'100px'}}>
                 <embed src={res.photo} width="100px" height="100px" />
             </td>
             <td style={{width:'150px'}}>
                 {
                     res.address
                 }
             </td>
             <td style={{width:'120px'}}>
                 {
                     res.city
                 }
             </td>
             <td>
                 {
                     res.zip
                 }
             </td>
             
             <td style={{width:'100px'}}>
             <embed src={res.resume} width="100px" height="100px" />
             </td>
             <td style={{width:'120px'}}>
                 {  
                     res.phno
                 }
             </td>
             <td style={{width:'150px'}}>
                 {
                     res.skill
                 }
             </td>
             <td style={{width:'150px'}}>
                 {
                     res.exp
                 }
             </td>
             <td>
                 {
                     res.ten
                 }
             </td>
             <td>
                 {
                     res.twelve
                 }
             </td>
             </tr>
        </tbody>);
        return (
            <div>
                <table style={{marginLeft:'20px'}} >
                    <tr id='head'>
                        <td>name</td>
                        <td>photo</td>
                        <td>address</td>
                        <td>city</td>
                        <td>zip</td>
                        <td>resume</td>
                        <td>phone no</td>
                        <td>skills</td>
                        <td>experience</td>
                        <td>10th</td>
                        <td>12th</td>
                    </tr>
                    {ad}
                </table>
            </div>
        )

    }

}

export default ShowData
