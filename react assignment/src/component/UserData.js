// import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'react-bootstrap';
import './UserData.css' ;
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
class UserData extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      name:'',
      city:'',
      photo:'',
      resume:'',
      address:'',
      phno:'',
      ten:'',
      twelve:'',
      exp:'',
      skill:'',
      route:false,
    }
    this.submit = this.submit.bind(this);
  }
  onphnochange=(e)=>{
    this.setState({
      phno:e.target.value
    })
  }
  onnamechange=(e)=>{
    this.setState({
      name:e.target.value
      })
  }
  oncitychange=(e)=>{
    this.setState({
      city:e.target.value
      })
  }
  onaddresschange=(e)=>{
    this.setState({
      address:e.target.value
      })
  }
  onzipchange=(e)=>{
    this.setState({
      zip:e.target.value
      })
    }
  onphotochange=(e)=>{
    let photo=e.target.files[0]
    let reader=new FileReader();
    reader.onload=(e)=>{
    this.setState({
    photo:reader.result
    })
    }
    reader.readAsDataURL(photo);
    console.log(this.state.photo)

  }
  onresumechange=(e)=>{
    let resume=e.target.files[0]
    let reader=new FileReader();
    reader.onload=(e)=>{
    this.setState({
    resume:reader.result
    })
    }
    reader.readAsDataURL(resume);
    console.log(this.state.resume)

  }
  // onXiiSchoolChange=(e)=>{
  //   this.setState({
  //   xiischool:e.target.value
  //   })
  //   }
  //   onXiiBoardChange=(e)=>{
  //   this.setState({
  //   xiiboard:e.target.value
  //   })
  //   }
    onXiiMarksChange=(e)=>{
    this.setState({
    twelve:e.target.value
    })
    }
    onXMarksChange=(e)=>{
      this.setState({
      ten:e.target.value
      })
      }
      onexpChange=(e)=>{
        this.setState({
        exp:e.target.value
        })
        }
        onskillChange=(e)=>{
          this.setState({
          skill:e.target.value
          })
          }
    submit(e){
      e.preventDefault();
      console.log(this.state);
      var data=localStorage.getItem('user');
      console.log(data);
      var userdata=data?JSON.parse(data):[];
      userdata.push(this.state);
      localStorage.setItem('user',JSON.stringify(userdata));
  
      this.setState({
        xiimarks:'',
        name:'',
        city:'',
        photo:'',
        resume:'',
        address:'',
        phno:'',
        ten:'',
        twelve:'',
        exp:'',
        skill:'',
        route:true,
      })
  
    }
  render() {
    
    return (
      
      <div>
           <h1>DATA FORM</h1>
           
    <Form onSubmit={this.submit}>
      <FormGroup row>
        
        <Col md={6}>
        <Label id='label' for="name" >Name</Label>
          <Input  value={this.state.name} type="text" name="name" id="name" placeholder="Enter name" onChange={this.onnamechange} required='true' />
        </Col>
        <Col md={6}>
        <Label sm={3} for="photo" >Photo</Label>
          <Input   type="file" name="photo" id="photo"  onChange={this.onphotochange} required='true' accept="application/pdf" />
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="address" sm={1}>Address</Label>
        <Col md={4}>
          <Input value={this.state.address} type="textarea" name="address" id="address" onChange={this.onaddresschange} required='true'/>
        </Col>
        <Col md={3}>
        <Label for="city" >city</Label>
        <Input value={this.state.city} type="text" name="city" id="city" placeholder="enter city "onChange={this.oncitychange} required='true' />
        </Col>
        <Col md={3}>
        <Label for="city" >zip</Label>
        <Input value={this.state.zip} type="number" name="city" id="city" placeholder="enter zip code "onChange={this.onzipchange} required='true' />
        </Col>
      </FormGroup>
      
      <FormGroup row >
       
        <Col md={6}>
        <Label for="resume"  sm={1}>Resume</Label>
          <Input sm={6} type="file" name="resume"id="resume"onChange={this.onresumechange} accept="application/pdf" required='true' />
        </Col>
        <Col md={5}>
        <Label  for="phno" >Phone No:</Label>
        <Input type="number" value={this.state.phno} name="phno" id="phno" value={this.state.phno} placeholder="Enter no" onChange={this.onphnochange}required='true'  />
        </Col>
      </FormGroup>
      <FormGroup row>
        </FormGroup>
      <FormGroup row>
        <Col md={6}>
        <Label id='label' for="ten" >10th</Label>
          <Input type="number" value={this.state.ten} name="ten" id="ten" placeholder="Enter 10th result" required='true' onChange={this.onXMarksChange}/>
        </Col>
        <Col md={5}>
        <Label id='label' for="twelve" >12th</Label>
          <Input type="number" value={this.state.twelve} name="twelve" id="twelve" placeholder="Enter 12th result" required='true' onChange={this.onXiiMarksChange}/>
        </Col>
        </FormGroup>
        <FormGroup row>
        <Col md={6}>
        <Label id='label' for="exp" >Experience</Label>
          <Input type="text" name="exp" value={this.state.exp} id="exp" placeholder="Enter experience" onChange={this.onexpChange}
          required='true' />
        </Col>
        <Col md={5}>
        <Label id='label' for="skill" >skills</Label>
          <Input type="text" name="skill" value={this.state.skill} id="skill" placeholder="Enter skills" required='true' onChange={this.onskillChange} />
        </Col>
        </FormGroup>
        <FormGroup row>
        <Col sm={{ size: 20, offset: 6}}>
          <Button type='submit'>Submit</Button>
        </Col>
      </FormGroup>
    </Form>
    <Button style={{marginLeft:'705px'}} type="submit"><Link to="/detail">Check data</Link></Button> 
    </div>
    )
    }
  }

export default UserData;